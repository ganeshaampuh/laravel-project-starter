window.formmodified = 0;
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// Tinymce
var mceEditor = tinymce.init({
    selector: '.mce-editor',
    height: 300,
    relative_urls: false,
    menubar: false,
    plugins: [
    	'advlist lists image hr pagebreak codemirror paste'
    ],
    content_css: [baseUrl+'/frontend/css/main.css', baseUrl+'/frontend/css/custom.css'],
    toolbar: 'image | styleselect | bold italic bullist numlist link blockquote alignleft aligncenter alignright alignjustify insertfile hr pagebreak outdent indent | code',
    codemirror: {
        indentOnInit: true, // Whether or not to indent code on init.
        path: 'CodeMirror', // Path to CodeMirror distribution
        config: {           // CodeMirror config object
           mode: 'htmlmixed',
           lineNumbers: true
        }
    },
    setup: function(editor) {
        editor.on('input propertychange change', function(e) {
            editor.save();
            readyToSave();
        });
   }
});

// Slugify
$('.post-form input[name="slug"]').slugify('.post-form input[name="title"]');

var timeoutId,
	form = $('.post-form');
	input = $('.post-form input, .post-form textarea, .post-form select'),
    btnSave = form.find('#save'),
	btnDelete = form.find('#delete');

input.on('input propertychange change', function() {
    formmodified = 1;
    readyToSave();
});

window.onbeforeunload = confirmExit;
function confirmExit() {
    if (formmodified == 1) {
        return "New information not saved. Do you wish to leave the page?";
    }
}

function readyToSave() {
	$(btnSave).prop('disabled', false);
}

form.on('submit', function(e) {
	e.preventDefault();

	var $this = $(this),
		url = $this.attr('action'),
		method = $this.attr('method'),
		btnSave = $this.find('#save'),
		data = $this.serialize(),
        enableButton = function() {
            btnSave.html('<i class="fa fa-save"></i> Save');
            btnSave.prop('disabled', false);
        },
        desableButton = function() {
            btnSave.text('Saving...');
            btnSave.prop('disabled', true);
        },
        showAlert = function() {
            $('#alert').fadeIn();
        },
        hideAlert = function() {
            $('#alert').fadeOut();
        },
        clearError = function() {
            $('[name]').each(function() {
                var formGroup = $(this).closest('.form-group');
                formGroup.removeClass('has-error');
                formGroup.find('.help-block').remove();
            });
        };

	desableButton();

	// Send AJAX
	$.ajax({
        url: url,
        type: method,
        data: data,
        success: function(result) {
            formmodified = 0;
            if(result.method == 'post') {
                window.location = url+'/'+result.data.id+'/edit';
            }
            clearError();
            hideAlert();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            formmodified = 0;
        	var errors = jqXHR.responseJSON;
            clearError();
        	for(var key in errors) {
        		var error = errors[key][0],
        			formGroup = $('[name="'+key+'"]').closest('.form-group');

        		formGroup.addClass('has-error');
        		formGroup.append('<span class="help-block">'+error+'</span>');

                showAlert();
        	}
        }
	}).always(function() {
	    enableButton();
	});
});

btnDelete.on('click', function(e) {
    e.preventDefault();

    var action = $(this).data('action');

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function() {
        $.ajax({
            url: action,
            type: 'DELETE',
            success: function(result) {
                swal(
                    'Deleted!',
                    'Your post has been deleted.',
                    'success'
                );

                window.location = '/admin/post';
            }
        });
    });
})


// $('.tags').selectize({
//     plugins: ['remove_button'],
//     delimiter: ',',
//     persist: false,
//     valueField: 'tag',
//     labelField: 'tag',
//     searchField: 'tag',
//     options: tags,
//     create: function(input) {
//         return {
//             tag: input
//         }
//     }
// });