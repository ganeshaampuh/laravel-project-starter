var finders = document.querySelectorAll('.finder-input'),
    btnSave = document.getElementById('save');

Array.prototype.forEach.call(finders, function(finder, i){
    var triggerButton = document.createElement('button'),
        removeButton = document.createElement('button'),
        modal = document.createElement('div'),
        modalContent = '<div class="finder-modal__overlay"></div>' +
                '<div class="finder-modal__main">' +
                    '<iframe src="'+baseUrl+'/finder"></iframe>' +
                    '<input type="hidden" name="src" id="src">' +
                    '<div class="finder-modal__footer">' +
                        '<button class="finder-modal__select" disabled>Select</button>' +
                        '<button class="finder-modal__close">Close</button>' +
                    '</div>' +
                '</div>';
    console.log(finder);
    modal.className = 'finder-modal';
    modal.innerHTML = modalContent;

    var btnSelect = modal.querySelector('.finder-modal__select'),
        btnClose = modal.querySelector('.finder-modal__close');

    triggerButton.className = 'finder-input__browse';
    triggerButton.textContent = 'Browse';

    triggerButton.addEventListener('click', function(e) {
        e.preventDefault();

        document.body.appendChild(modal);
    });

    removeButton.className = 'finder-input__remove';
    removeButton.textContent = 'Remove';

    btnSelect.addEventListener('click', function(e) {
        e.preventDefault();

        var file = document.getElementById('src'),
            img = document.createElement('img'),
            input = finder.querySelector('input');

        img.src = baseUrl+'/finder/images/'+file.value;

        var currentImage = finder.querySelector('img');

        if(currentImage && input.value) {
            finder.removeChild(currentImage);
            input.value = null;
        }

        finder.appendChild(removeButton);
        finder.appendChild(img);
        input.value = file.value;

        triggerButton.textContent = 'Change';

        btnSave.disabled = false;

        document.body.removeChild(modal);
    });

    removeButton.addEventListener('click', function(e) {
        e.preventDefault();

        var img = finder.querySelector('img'),
            input = finder.querySelector('input');

        finder.removeChild(img);
        input.value = null;
        finder.removeChild(this);

        triggerButton.textContent = 'Browse';
        btnSave.disabled = false;
    });

    btnClose.addEventListener('click', function(e) {
        e.preventDefault();

        document.body.removeChild(modal);
    });

    finder.appendChild(triggerButton);

    function existImage() {
        var img = document.createElement('img'),
            input = finder.querySelector('input');

        if(input.value) {
            img.src = baseUrl+'/finder/images/'+input.value;
            finder.appendChild(removeButton);
            finder.appendChild(img);

            triggerButton.textContent = 'Change';
        }
    }

    existImage();
});
