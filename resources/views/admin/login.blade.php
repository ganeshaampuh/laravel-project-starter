<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ config('app.name') }} Admin | Sign In</title>

        <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700" rel="stylesheet">

    <!-- Vendor Styles -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    
    <!-- Styles -->
	<link rel="stylesheet" href="{{ mix('css/admin.css') }}">
</head>
<body>
	<!-- start: page -->
    <section class="body-sign">
        <div class="center-sign">
            <a href="#" class="logo pull-left">
                <img src="{{ asset('img/admin/logo.png') }}" height="54" alt="Logo Admin" />
            </a>

            <div class="panel panel-sign">
                <div class="panel-title-sign mt-xl text-right">
                    <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Sign In</h2>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => 'admin/login', 'method' => 'POST']) !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} mb-lg">
                            <label>Email</label>
                            <div class="input-group input-group-icon">
                                {!! Form::email('email', null, ['class' => 'form-control input-lg', 'placeholder' => 'Email']) !!}
                                <span class="input-group-addon">
                                    <span class="icon">
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                </span>
                            </div>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label>Password</label>

                            <div class="input-group input-group-icon">
                                {!! Form::password('password', ['class' => 'form-control input-lg', 'placeholder' => 'Password']) !!}
                                <span class="input-group-addon">
                                    <span class="icon icon-lg">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </span>
                            </div>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-sm-8">
                                <div class="checkbox-custom checkbox-default">
                                    <input id="RememberMe" name="remember" type="checkbox"/>
                                    <label for="RememberMe">Remember Me</label>
                                </div>
                            </div>
                            <div class="col-sm-4 text-right">
                                <button type="submit" class="btn btn-primary hidden-xs">Sign In</button>
                                <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>

            <p class="text-center text-muted mt-md mb-md">&copy; Copyright {{ date('Y', time()) }}. All Rights Reserved.</p>
        </div>
    </section>
    <!-- end: page -->
</body>
</html>