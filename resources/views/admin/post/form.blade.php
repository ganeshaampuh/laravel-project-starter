@extends('admin._layout')

@section('form-open')
	{!! Form::model($post, [
		'class' => 'post-form',
        'method' => $post->exists ? 'put' : 'post',
        'url' => $post->exists ? 'admin/post/'.$post->id : 'admin/post',
        'files' => 'true'
    ]) !!}
@endsection

@section('page-header')
	{!! Form::header($post, 'post') !!}
@endsection

@section('content')
	{!! Form::hidden('user_id', Auth::guard('admin')->id()) !!}
	<div id="alert" class="alert alert-danger alert-dismissible" role="alert" style="display: none;">
	  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  	<strong>Whoops,</strong> an error occurred!
	</div>

	<div class="tabs">
		<ul class="nav nav-tabs nav-justified">
			<li class="active">
				<a href="#content" data-toggle="tab" class="text-center" aria-expanded="true">Content</a>
			</li>
			<li class="">
				<a href="#excerpt" data-toggle="tab" class="text-center" aria-expanded="false">Options</a>
			</li>
			<li class="">
				<a href="#seo" data-toggle="tab" class="text-center" aria-expanded="false">Seo</a>
			</li>
			{{-- <li class="">
				<a href="#settings" data-toggle="tab" class="text-center" aria-expanded="false">Settings</a>
			</li> --}}
		</ul>
		<div class="tab-content">
			<div id="content" class="tab-pane active">
				<div class="form-group">
					<label class="control-label">Title *</label>
					{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
				</div>

				<div class="form-group">
					<label class="control-label">Url</label>
					<div class="input-group input-group-sm">
					  	<span class="input-group-addon">{{ url('/').'/' }}</span>
					  	{!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Url']) !!}
					</div>
				</div>

				<div class="form-group">
					<label class="control-label">Image *</label>
					{!! Form::finder('banner') !!}
				</div>

				<div class="form-group">
					<label class="control-label">Content *</label>
					{!! Form::tinymce('content', null, ['placeholder' => 'Content']) !!}
				</div>
			</div>
			<div id="excerpt" class="tab-pane">
				<div class="form-group">
					<label class="control-label">Thumbnail</label>
					{!! Form::finder('thumbnail') !!}
				</div>
				<div class="form-group">
					<label class="control-label">Excerpt</label>
					{!! Form::textarea('excerpt', null, ['class' => 'form-control', 'placeholder' => 'Excerpt', 'rows' => 4]) !!}
				</div>
				{{-- <div class="form-group">
					<label class="control-label">Categories</label>
				</div>
				<div class="form-group">
					<label class="control-label">Tags</label>
					{!! Form::text('tag_names', null, ['class' => 'tags', 'placeholder' => 'Tags']) !!}
					{{ $post->tag_names }}
				</div> --}}
			</div>
			<div id="seo" class="tab-pane">
				{!! Form::meta() !!}
			</div>
			{{-- <div id="settings" class="tab-pane">
				<p>Recent <code>.nav-tabs.nav-justified</code></p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>
			</div> --}}
		</div>
	</div>
@endsection

@section('form-close')
	{!! Form::close() !!}
@endsection

@section('styles')
  	<link href="{{ asset('vendor/finder/css/finder.css') }}" rel="stylesheet">
@endsection

{{--
	var tags = [
        @foreach (tags() as $tag)
        {tag: "{{$tag}}" },
        @endforeach
    ];
--}}

@section('scripts')
	<script>
		window.baseUrl = "{{ url('/') }}";
  	</script>
  	<script src="{{ asset('js/admin.post.js') }}"></script>
  	<script src="{{ asset('js/finder.js') }}"></script>
@endsection