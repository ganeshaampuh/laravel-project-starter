@extends('admin._layout')

@section('styles')
<style>
	.order-by {
		display: inline-block;
		width: 50%;
		vertical-align: middle;
	}
</style>
@endsection

@section('page-header')
	<div class="post-toolbar">
		<div class="row">
			<div class="col-md-5">
				<div class="row">
					<div class="col-md-6">
						<select name="order_by" class="form-control order-by">
					    	<option value="">Sort By</option>
					    	<option value="title-asc">Sort By Post (A-Z)</option>
					    	<option value="title-desc">Sort By Post (Z-A)</option>
					    	<option value="created_at-desc">Sort By Latest</option>
					    	<option value="created_at-asc">Sort By Oldest</option>
					    </select>
						<a href="{{ url('admin/post/create') }}" class="btn btn-primary btn-sm">
							<i class="fa fa-plus"></i> Add New
						</a>
					</div>
					<div class="col-md-6">
						<form action="{{ request()->url() }}">
							<div class="input-group">
						      	<input type="text" name="keyword" class="form-control" placeholder="Search for..." value="{{ request()->keyword }}">
						      	<span class="input-group-btn">
						        	<button class="btn btn-default btn-sm" type="submit"><i class="fa fa-search"></i></button>
						      	</span>
						    </div><!-- /input-group -->
					    </form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('content')
	<posts
		:posts="{{ $posts }}"
		:can-edit="1"
		:can-delete="1"></posts>
@endsection

@section('scripts')
<script>
	$('select[name=order_by]').on('change', function(){
		var value = $(this).val();
		window.location = "{{ request()->url() }}?order_by="+value;
	});
</script>
@endsection