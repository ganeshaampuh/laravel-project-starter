<!DOCTYPE html>
<html class="fixed sidebar-left-xs" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title or 'Admin' }}</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor Styles -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/nanoscroller/nanoscroller.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/dropify/css/dropify.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/selectize/css/selectize.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/cubeportfolio/css/cubeportfolio.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/sweetalert2/4.0.11/sweetalert2.min.css" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    @yield('styles')

    <!-- Head Libs -->
    <script src="{{ asset('vendor/modernizr/modernizr.js') }}"></script>
    <script>
        window.baseUrl = "{{ url('/') }}";
    </script>
</head>
<body>
    <section class="body">
        <!-- start: header -->
        @include('admin.partials.header')
        <!-- end: header -->

        <div class="inner-wrapper">
            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left">

                <div class="sidebar-header">
                    <div class="sidebar-title">
                        Navigation
                    </div>
                </div>

                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                            @include('admin.partials.menu')
                        </nav>
                    </div>

                </div>

            </aside>
            <!-- end: sidebar -->

            <section role="main" class="content-body">
                @yield('form-open')
                <header class="page-header">
                    @yield('page-header')
                </header>

                <!-- start: page -->
                @yield('content')
                <!-- end: page -->
                @yield('form-close')
            </section>
        </div>

    </section>

    <!-- Vendor JavaScripts -->
    <script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
    <script src="{{ asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('vendor/nanoscroller/nanoscroller.js') }}"></script>
    <script src="{{ asset('vendor/jquery-placeholder/jquery-placeholder.js') }}"></script>
    <script src="{{ asset('vendor/dropify/js/dropify.js') }}"></script>
    <script src="{{ asset('vendor/selectize/js/selectize.js') }}"></script>
    <script src="{{ asset('vendor/cubeportfolio/js/jquery.cubeportfolio.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-daterangepicker/moment.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('vendor/slugify/speakingurl.min.js') }}"></script>
    <script src="{{ asset('vendor/slugify/slugify.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/sweetalert2/4.0.11/sweetalert2.min.js"></script>

    <!-- JavaScripts -->
    <script src="{{ asset('js/admin.theme.js') }}"></script>
    <script src="{{ asset('js/admin.theme.init.js') }}"></script>
    <script src="{{ asset('js/vue-tables.min.js') }}"></script>
    <script src="{{ asset('js/admin.js') }}"></script>
    @yield('scripts')
</body>
</html>