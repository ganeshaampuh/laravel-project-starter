@extends('admin._layout')

@section('styles')
<style>
	.VueTables {
		margin-top: -35px;
	}
	.VueTables__action-filter-wrapper {
		display: none;
	}
</style>
@endsection

@section('content')
	<a href="{{ url('admin/user/create') }}" class="btn btn-primary">Add new user</a>
	<v-server-table url="{{ request()->url() }}" :columns="['name', 'email', 'action']" :options="{
      	headings: {
	        email: 'Email Address'
      	},
		filterByColumn: true,
		sortable: ['name', 'email']
	}"></v-server-table>
@endsection

@section('scripts')
<script>
	$(document).on('click', '.action__delete button', function(e) {
		e.preventDefault();
		if(confirm('Are you sure want to delete this subscriber?')) {
			$(this).parent().submit();
		}
	});
</script>
@endsection