@extends('admin._layout')

@section('form-open')
	{!! Form::model($user, [
        'method' => $user->exists ? 'put' : 'post',
        'url' => $user->exists ? 'admin/user/'.$user->id : 'admin/user'
    ]) !!}
@endsection

@section('page-header')
	<div class="post-form__actions">
		<a id="back" href="{{ url('admin/user') }}" class="back">
			<svg class="svg-icon" viewBox="0 0 20 20">
				<path fill="none" d="M18.271,9.212H3.615l4.184-4.184c0.306-0.306,0.306-0.801,0-1.107c-0.306-0.306-0.801-0.306-1.107,0
				L1.21,9.403C1.194,9.417,1.174,9.421,1.158,9.437c-0.181,0.181-0.242,0.425-0.209,0.66c0.005,0.038,0.012,0.071,0.022,0.109
				c0.028,0.098,0.075,0.188,0.142,0.271c0.021,0.026,0.021,0.061,0.045,0.085c0.015,0.016,0.034,0.02,0.05,0.033l5.484,5.483
				c0.306,0.307,0.801,0.307,1.107,0c0.306-0.305,0.306-0.801,0-1.105l-4.184-4.185h14.656c0.436,0,0.788-0.353,0.788-0.788
				S18.707,9.212,18.271,9.212z"></path>
			</svg>
		</a>

		<div class="post-form__divider"></div>

		<button id="save" class="btn btn-success btn-sm mr-sm" type="submit"><i class="fa fa-save"></i> Save</button>

	</div>
@endsection

@section('content')
	<div style="max-width: 700px;margin: auto;">
		<div id="alert" class="alert alert-danger alert-dismissible" role="alert" style="display: none;">
		  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  	<strong>Whoops,</strong> an error occurred!
		</div>
		<div class="row mb-sm">
			<div class="col-md-6">
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		            <label for="name" class="control-label">Name</label>

		            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}

	                @if ($errors->has('name'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('name') }}</strong>
	                    </span>
	                @endif
		        </div>
			</div>
			<div class="col-md-6">
				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	                <label for="email" class="control-label">E-Mail Address</label>
					<?php
						$attr = ['class' => 'form-control', 'placeholder' => 'Email'];
						if($user->exists) {
							$attr['readonly'] = 'readonly';
						}
					?>
	                {!! Form::email('email', null, $attr) !!}

	                @if ($errors->has('email'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('email') }}</strong>
	                    </span>
	                @endif
	            </div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
	                <label for="password" class="control-label">Password</label>

	                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}

	                @if ($errors->has('password'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('password') }}</strong>
	                    </span>
	                @endif
	            </div>
			</div>
			<div class="col-md-6">
				<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
	                <label for="password-confirm" class="control-label">Confirm Password</label>

	                {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password']) !!}

	                @if ($errors->has('password_confirmation'))
	                    <span class="help-block">
	                        <strong>{{ $errors->first('password_confirmation') }}</strong>
	                    </span>
	                @endif
	            </div>
			</div>
		</div>
		<hr>
		<label for="">Give permissions to user</label>
		<div class="row">
			<div class="col-md-3">
				<strong>Article</strong>
				<ul style="list-style: none;">
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'view article', null, ['id' => 'p1']) !!}
							<label for="p1">View</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'create article', null, ['id' => 'p2']) !!}
							<label for="p2">Create</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'edit article', null, ['id' => 'p3']) !!}
							<label for="p3">Edit</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'delete article', null, ['id' => 'p4']) !!}
							<label for="p4">Delete</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'publish article', null, ['id' => 'p5']) !!}
							<label for="p5">Publish</label>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-md-3">
				<strong>Banner</strong>
				<ul style="list-style: none;">
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'view banner', null, ['id' => 'p6']) !!}
							<label for="p6">View</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'create banner', null, ['id' => 'p7']) !!}
							<label for="p7">Create</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'edit banner', null, ['id' => 'p8']) !!}
							<label for="p8">Edit</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'delete banner', null, ['id' => 'p9']) !!}
							<label for="p9">Delete</label>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-md-3">
				<strong>Job</strong>
				<ul style="list-style: none;">
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'view job', null, ['id' => 'p11']) !!}
							<label for="p11">View</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'create job', null, ['id' => 'p12']) !!}
							<label for="p12">Create</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'edit job', null, ['id' => 'p13']) !!}
							<label for="p13">Edit</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'delete job', null, ['id' => 'p14']) !!}
							<label for="p14">Delete</label>
						</div>
					</li>
					<li class="mt-xs"><strong><small>Applicant</small></strong></li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'view applicant', null, ['id' => 'p10']) !!}
							<label for="p10">View</label>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-md-3">
				<strong>Newsletter</strong>
				<ul style="list-style: none;">
					<li class="mt-xs"><strong><small>Subscribers</small></strong></li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'view subscribers', null, ['id' => 'p15']) !!}
							<label for="p15">View</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'create subscribers', null, ['id' => 'p16']) !!}
							<label for="p16">Create</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'edit subscribers', null, ['id' => 'p17']) !!}
							<label for="p17">Edit</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'delete subscribers', null, ['id' => 'p18']) !!}
							<label for="p18">Delete</label>
						</div>
					</li>
					<li class="mt-xs"><strong><small>Campaign</small></strong></li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'create campaign', null, ['id' => 'p19']) !!}
							<label for="p19">Create</label>
						</div>
					</li>
					<li>
						<div class="checkbox-custom checkbox-default">
							{!! Form::checkbox('permissions[]', 'delete campaign', null, ['id' => 'p20']) !!}
							<label for="p20">Delete</label>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
@endsection

@section('form-close')
	{!! Form::close() !!}
@endsection

@section('styles')
  	<link href="{{ asset('vendor/finder/css/finder.css') }}" rel="stylesheet">
@endsection

{{--
	var tags = [
        @foreach (tags() as $tag)
        {tag: "{{$tag}}" },
        @endforeach
    ];
--}}

@section('scripts')
	<script>
		window.baseUrl = "{{ url('/') }}";
  	</script>
  	<script src="{{ asset('backend/js/post.js') }}"></script>
  	<script src="{{ asset('backend/js/finder.js') }}"></script>
@endsection