<ul class="nav nav-main">
	<li @if(nav_is_active('admin')) class="nav-active" @endif>
		<a href="{{ url('admin') }}">
			<i class="fa fa-home" aria-hidden="true"></i>
			<span>Dashboard</span>
		</a>
	</li>
	<li @if(nav_is_active('admin/post')) class="nav-active" @endif>
		<a href="{{ url('admin/post') }}">
			<i class="fa fa-file-o" aria-hidden="true"></i>
			<span>Article</span>
		</a>
	</li>
	{{-- @if(Auth::guard('admin')->user()->id == 1)
	<li @if(nav_is_active('user')) class="nav-active" @endif>
		<a href="{{ url('admin/user') }}">
			<i class="fa fa-user" aria-hidden="true"></i>
			<span>User</span>
		</a>
	</li>
	@endif --}}
</ul>