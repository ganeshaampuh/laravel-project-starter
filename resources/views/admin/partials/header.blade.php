<header class="header">
    <div class="logo-container">
        <a href="{{ url('admin') }}" class="logo">
            <img src="{{ asset('img/admin/logo.png') }}" height="35" alt="CK3 Admin" />
        </a>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
        <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <!-- start: search & user box -->
    <div class="header-right">
        <span class="separator"></span>
        @if(auth('admin')->check())
        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="{{ url('admin/user/avatar') }}" alt="{{ auth('admin')->user()->name }}" class="img-circle" data-lock-picture="{{ url('admin/user/avatar') }}" />
                </figure>

                <div class="profile-info" data-lock-name="{{ auth('admin')->user()->name }}">
                    <span class="name">{{ auth('admin')->user()->name }}</span>
                    {{-- <span class="role">administrator</span> --}}
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu">
                <ul class="list-unstyled">
                    <li class="divider"></li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="{{ url('/') }}" target="_blank"><i class="fa fa-file-o"></i> View Site</a>
                    </li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="{{ url('admin/logout') }}"><i class="fa fa-power-off"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
        @endif
    </div>
    <!-- end: search & user box -->
</header>