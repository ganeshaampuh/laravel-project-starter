<div class="options">
	<div class="row">
		@foreach($optionsArray as $value => $label)
		<div class="col-md-{{ $col }}">
			<div class="radio radio-primary">
				{!! Form::radio($optName, $value, null, array_merge(['id' => $optName.'_'.Illuminate\Support\Str::slug($value)], $options)) !!}
			  	<label for="{{ $optName.'_'.Illuminate\Support\Str::slug($value) }}">{{ $label }}</label>
			</div>
		</div>
		@endforeach
		@if($other)
		<div class="col-md-4">
			<div class="radio radio-primary" style="display: inline-block;">
				{!! Form::radio($optName, 'Other', null, array_merge(['id' => $optName.'_other'], $options)) !!}
			  	<label for="{{ $optName.'_other' }}">Other</label>
			</div>
			{!! Form::hidden($name, null, [
				'class' => 'form-control other-option-value',
				'placeholder' => 'Enter Your Other Value',
				'style' => 'display: inline-block;width:auto;min-width: 200px;margin-left: 15px;margin-top: 0px;',
				'required'
			]) !!}
		</div>
		@else
		{!! Form::hidden($name, null, [
			'class' => 'form-control other-option-value',
			'placeholder' => 'Enter Your Other Value',
			'style' => 'display: inline-block;width:auto;min-width: 200px;margin-left: 15px;margin-top: 0px;',
			'required'
		]) !!}
		@endif
	</div>
</div>