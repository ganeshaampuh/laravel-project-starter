<div class="form-group">
	<label class="control-label">Title (Meta Title)</label>
	{!! Form::text('meta[title]', null, ['class' => 'form-control', 'placeholder' => 'Meta Title']) !!}
</div>
<div class="form-group">
	<label class="control-label">Description (Meta Description)</label>
	{!! Form::textarea('meta[description]', null, ['class' => 'form-control', 'placeholder' => 'Meta Description', 'rows' => 4]) !!}
</div>
<div class="form-group">
	<label class="control-label">Keywords (Meta Keywords)</label>
	{!! Form::text('meta[keywords]', null, ['class' => 'form-control', 'placeholder' => 'Meta Keywords']) !!}
</div>
<div class="form-group">
	<label class="control-label">Canonical URL (Canonical Tag)</label>
	{!! Form::text('meta[canonical]', null, ['class' => 'form-control', 'placeholder' => 'Canonical Tag']) !!}
</div>