<?php

// Dashboard
Route::get('/', 'Admin\DashboardController@index');

// Authenticate Route
Route::get('login', 'Admin\SessionController@create');
Route::post('login', 'Admin\SessionController@store');
Route::post('logout', 'Admin\SessionController@destroy');

// User
Route::get('user/avatar', 'Admin\UserController@avatar');
Route::resource('user', 'Admin\UserController');

// Post
Route::resource('post', 'Admin\PostController');