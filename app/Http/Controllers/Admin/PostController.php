<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;

class PostController extends Controller
{

    public function __construct()
    {
        //$this->middleware('localize-post');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = new Post;
        if(request()->has('keyword')){
            $posts = $posts->where('title', 'like', '%'.request()->keyword.'%');
        }

        $order_field = 'created_at';
        $order_direction = 'desc';

        if(request()->has('order_by')) {
            $order_by = explode('-', request()->order_by);
            $order_field = $order_by[0];
            $order_direction = $order_by[1];
        }

        $posts = $posts->orderBy($order_field, $order_direction)->get();
        return view('admin.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Post $post)
    {
        return view('admin.post.form', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PostRequest $request)
    {
        $post = Post::create($request->all());

        return response()->json(['status' => 'success', 'method' => 'post', 'data' => $post], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.post.form', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\PostRequest $request, $id)
    {
        $post = Post::findOrFail($id);
        // if($request->has('locale')) {
        //     $post->translate($request->locale);
        // }
        $post->update($request->all());

        return response()->json(['status' => 'success', 'method' => 'put', 'data' => $post], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return response()->json('success', 200);
    }
}
