<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Route;

abstract class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get a parameter of the current route.
     *
     * @param $key
     *
     * @return object|string
     */
    protected function getRouteParameter($key)
    {
        return Route::getCurrentRoute()->parameter($key);
    }
}
