<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:120', //|unique:post_translations,title
            'slug' => 'required|max:120', //|unique:post_translations,slug
        ];

        // if (in_array($this->method(), ['PUT', 'PATCH'])) {
        //     $rules['title'] .= ",{$this->getRouteParameter('post')}";
        //     $rules['slug'] .= ",{$this->getRouteParameter('post')}";
        // }

        if($this->has('status') and $this->status == 1) {
            $rules['image'] = 'required';
            $rules['content'] = 'required';
        }

        return $rules;
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages($value='')
    {
        $messages = [
            'slug.required' => 'The url field is required.',
            'slug.unique' => 'The url has already been taken.'
        ];

        return $messages;
    }
}
