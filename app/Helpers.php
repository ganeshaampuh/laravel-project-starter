<?php

use Conner\Tagging\Model\Tag;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

if(!function_exists('nav_is_active')) {
	/**
	 * check nav is active
	 * @param  string $nav
	 * @return boolean
	 */
    function nav_is_active($nav){
        if(request()->is($nav)) return true;

        return false;
    }
}

if (!function_exists('tags')) {
	/**
	 * get existing tags from model
	 * @param  Model $model
	 * @return array
	 */
	function tags() {
		return Tag::pluck('name');
	}
}

function nav($nav, $class = 'menu__item--active')
{
	if(request()->is('*/'.$nav) or ($nav == 'home' and request()->url() == url(request()->segment(1)) )) {
		return $class;
	}
}

if(!function_exists('human_filesize'))
{
	function human_filesize($bytes, $decimals = 2) {
	    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
	    $factor = floor((strlen($bytes) - 1) / 3);
	    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
	}
}

if(!function_exists('locale')) {
	function locale($locale = null)
	{
		return LaravelLocalization::setLocale($locale);
	}
}
if(!function_exists('localizedURL')) {
	function localizedURL($locale = null, $model = null)
	{
		$url = null;
		if(!empty($model)){
			$currentRoute = Route::current();
			$parameters = [];
			if(!empty($currentRoute->parameters())) {
				foreach ($currentRoute->parameters() as $key => $value) {
					if(!empty($model->translate($locale))) {
						$parameters[] = $model->translate($locale)->{$key};
					} else {
						$parameters[] = $model->{$key};
					}
				}
			}
			$url = route($currentRoute->getName(), $parameters);
		}
		return LaravelLocalization::getLocalizedURL($locale, $url);
	}
}