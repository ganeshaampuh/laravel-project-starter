<?php

namespace App\Services\VueTables;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

Class EloquentVueTables  implements VueTablesInterface  {

  public function get($model, Array $fields, Array $joins = [], Array $filters = []) {

    extract(request()->only('query', 'limit', 'page', 'orderBy', 'ascending', 'byColumn'));

    $data = $model;

    if(!empty($joins)) {
      foreach ($joins as $join) {
        $table = $join;
        $foreignKey = str_singular($model->getTable()).'_id';
        $refId = 'id';
        if(str_contains($join, ',')) {
          $join = explode(',', $join);
          $table = $join[0];
          $foreignKey = $join[1];
          if(array_key_exists(2, $join)) {
            $refId = $join[2];
          }
        }
        $data = $data->leftJoin($table, $model->getTable().'.'.$refId, '=', $table.'.'.$foreignKey)->groupBy($model->getTable().'.id');
      }
      $fields = array_map(function($i) use ($model) {
        if(str_contains($i, ':')) {
          $rawQuery = explode(':', $i)[1];
          return DB::raw($rawQuery);
        }
        return str_contains($i, '.') ? $i : $model->getTable().'.'.$i;
      }, $fields);
      if (isset($query) && $query) {
        foreach ($fields as $key) {
          if(str_contains($key, '.')) {
            $xkey = explode('.', $key);
            if(array_key_exists($xkey[1], $query)) {
              $query[$key] = $query[$xkey[1]];
              unset($query[$xkey[1]]);
            }

            if($xkey[1] == $orderBy) {
              $orderBy = $key;
            }
          }
        }
      }
    }

    if(!empty($filters)) {
      $data = $data->where($filters);
    }

    $data = $data->select($fields);

    if (isset($query) && $query) {
       $data = $byColumn==1?$this->filterByColumn($data, $query):
                           $this->filter($data, $query, $fields);
  }

  $data->limit($limit)
  ->skip($limit * ($page-1));

  if (isset($orderBy) && $orderBy):
    $direction = $ascending==1?"ASC":"DESC";
  $data->orderBy($orderBy,$direction);
  endif;

  $results = $data->get()->toArray();

  return ['data'=>$results,
          'count'=>count($results)];

}

protected function filterByColumn($data, $query) {
  foreach ($query as $field=>$query):

    if (!$query) continue;

  if (is_string($query)) {
   $data->where($field,'LIKE',"%{$query}%");
 } else {

  $start = Carbon::createFromFormat('Y-m-d',$query['start'])->startOfDay();
  $end = Carbon::createFromFormat('Y-m-d',$query['end'])->endOfDay();

  $data->whereBetween($field,[$start, $end]);
}
endforeach;

return $data;
}

protected function filter($data, $query, $fields) {
  foreach ($fields as $index=>$field):
    $method = $index?"orWhere":"where";
  $data->{$method}($field,'LIKE',"%{$query}%");
  endforeach;

  return $data;
}

}
