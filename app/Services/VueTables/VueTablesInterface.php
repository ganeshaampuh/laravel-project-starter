<?php
/**
 *  VueTables server-side component interface
 */

namespace App\Services\VueTables;

use Illuminate\Database\Eloquent\Model;

Interface VueTablesInterface {

  public function get($model, Array $fields);

}
