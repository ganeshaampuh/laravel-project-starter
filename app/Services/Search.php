<?php

namespace App\Services;

use App\Models\Job;
use App\Models\Post;
use Illuminate\Support\Str;

class Search
{
	protected $keyword;

	public function get($keyword = '')
	{
		$this->keyword = $keyword;
		return [
			'posts' => $this->searchArticle(),
			'jobs' => $this->searchjob(),
			'count' => count($this->searchArticle()) + count($this->searchjob())
		];
	}

	protected function searchArticle()
	{
		$result = Post::where('status', 1)
						->whereTranslationLike('title', '%'.$this->keyword.'%')
						->whereTranslationLike('content', '%'.$this->keyword.'%')
						->take(10)
						->get();
		return collect($result)->map(function($item) {
			$item->title = $item->title;
			$item->content = Str::limit(trim(strip_tags($item->content)), 300);
			return $item;
		});
	}

	protected function searchjob()
	{
		return Job::where('status', 1)
					->where('title', 'like', '%'.$this->keyword.'%')
					->orWhere('content', 'like', '%'.$this->keyword.'%')
					->take(10)
					->get();
	}

	protected function highlightMatch($text) {
	    return preg_replace('/\p{L}*?'.preg_quote($this->keyword).'\p{L}*/ui', '<span class="highlight">$0</span>', $text);
	}

	protected function getMatchContent($text) {
		preg_match('/^.*?'.$this->keyword.'.*?$/imU', trim(strip_tags($text)), $matches);
		return $matches;
	}
}