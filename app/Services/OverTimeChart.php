<?php

namespace App\Services;

class OverTimeChart {
	protected $campaign;

	public function __construct($campaign)
	{
		$this->campaign = $campaign;
	}

	public function get()
	{
		return [
			'categories' => $this->categories(),
			'data' => $this->data(),
		];
	}

	protected function dateTime($i = 1)
	{
		$dateTime = $this->campaign->created_at;
		$dateTime->hour += $i;
        $dateTime->minute = 0;
        $dateTime->second = 0;
        return $dateTime;
	}

	protected function categories()
	{
		$categories = [];
		for ($i=0; $i <= 24; $i++) {
            $dateTime = $this->dateTime($i);
            $categories[] = $dateTime->format('d M, ga');
        }
        return $categories;
	}

	protected function data()
	{
		$data = [];
		for ($i=0; $i <= 24; $i++) {
            $dateTime = $this->dateTime($i);
            $start = $dateTime->toDateTimeString();
            $end = $dateTime->addHour()->subSecond()->toDateTimeString();
            $count = 0;
            $query = $this->campaign->sents()->whereBetween('created_at', [$start, $end])->get();
            foreach ($query as $value) {
	            $count += $value->opens;
	        }
	        $data[] = $count;
        }
        return $data;
	}
}