<?php

namespace App\Services\Html;

use Collective\Html\FormBuilder as CollectiveFormBuilder;

class FormBuilder extends CollectiveFormBuilder
{

	public function tinymce($name, $value = null, $options = [])
	{
		$options['class'] = 'mce-editor' . (isset($options['class']) ? ' ' . $options['class'] : '');
		return parent::textarea($name, $value, $options);
	}

	public function header($model, $endpoint = null)
	{
		return view('components.form.header', compact('model', 'endpoint'));
	}

	public function meta()
	{
		return view('components.form.meta');
	}

	public function finder($name, $value = null)
	{
		return view('components.form.finder', compact('name', 'value'));
	}

	public function options($name, $optionsArray, $other = false, $options = [])
	{
		$row = 12;
		if($other){ $row -= 4; }
		$col = round($row / count($optionsArray));
		$optName = 'option_'.str_random(6);
		return view('components.form.options', compact('name', 'optionsArray', 'other', 'options', 'col', 'optName'));
	}

}