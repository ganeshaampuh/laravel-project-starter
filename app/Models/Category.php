<?php

namespace App\Models;

class Category extends Model
{
    public function posts()
    {
    	return $this->hasMany(Post::class);
    }
}
