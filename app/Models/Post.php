<?php

namespace App\Models;

class Post extends Model
{
	protected $casts = [
        'meta' => 'json',
    ];

	public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function tags()
    {
    	return $this->belongsToMany(Tag::class);
    }
}
